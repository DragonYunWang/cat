import { useState, useEffect } from 'react'

const useTimeout = ms => {
  const [ready, setReady] = useState(false)

  useEffect(() => {
    let timer = setTimeout(() => {
      setReady(true)
    }, ms)

    return () => {
      clearTimeout(timer)
    }
  }, [ms])

  return ready
}

export default useTimeout
