/**
 *序列化查询参数
 *
 * @export
 * @param {*} data
 * @returns url
 */
export function param(data) {
  let url = ""
  for (const k in data) {
    let value = data[k] !== undefined ? data[k] : ""
    url += "&" + k + "=" + encodeURIComponent(value)
  }
  return url ? "?" + url.substring(1) : ""
}

/**
 * 解析url参数
 * @example ?id=12345&a=b
 * @return Object {id:12345,a:b}
 */
export function urlParse() {
  let url = window.location.hash
  let obj = {}
  let reg = /[?&][^?&]+=[^?&]+/g
  let arr = url.match(reg)
  // ['?id=12345', '&a=b']
  if (arr) {
    arr.forEach(item => {
      let tempArr = item.substring(1).split("=")
      let key = decodeURIComponent(tempArr[0])
      let val = decodeURIComponent(tempArr[1])
      obj[key] = val
    })
  }
  return obj
}

/**
 *
 * 判断是否是浏览器
 * @export is_weixn
 * @returns boolen
 */
export function is_weixn() {
  var ua = navigator.userAgent.toLowerCase()
  // eslint-disable-next-line
  return ua.match(/MicroMessenger/i) == "micromessenger"
}

export function parseHeaders(headers) {
  let parsed = Object.create(null)

  if (!headers) {
    return parsed
  }

  function reducer(accumulator, currentValue) {
    let [key, val] = currentValue.split(":")

    key = key.trim().toLowerCase()

    if (!key) {
      return accumulator
    }

    if (val) {
      val = val.trim()
    }

    accumulator[key] = val

    return accumulator
  }

  return headers.split("\r\n").reduce(reducer, parsed)
}

export function fetchSimilarHeaders() {
  return new Promise(resolve => {
    var request = new XMLHttpRequest()
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE) {
        console.log(request.getAllResponseHeaders(), 'request.getAllResponseHeaders()')
        resolve(parseHeaders(request.getAllResponseHeaders()))
      }
    }
    request.open("HEAD", document.location, true)
    request.send(null)
  })
}
