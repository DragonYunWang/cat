// 环境变量优先级
// npm start: .env.development.local, .env.development, .env.local, .env
// npm run build: .env.production.local, .env.production, .env.local, .env
// npm test: .env.test.local, .env.test, .env (note .env.local is missing)
export const CAT_API = process.env.REACT_APP_API_CAT
export const CODE_URL = process.env.REACT_APP_CODE_URL
export const link = "http://oss-t.mhuzi.com/index/huzijing/index.html"
