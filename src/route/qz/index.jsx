import React, { PureComponent } from "react"
import { Modal, ImagePicker, Toast } from "antd-mobile"
import { urlParse, fetchSimilarHeaders } from "../../util/util"
import { getEWMImg, uploadEWM } from "../../api"
import { link } from "../../constants/conf"
import "./index.scss"

class QZ extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      state: undefined,
      files: []
    }
  }

  async componentDidMount() {
    const { location } = this.props
    const params = urlParse(location.search.split("?"))
    const { qId } = params
    const headers = await fetchSimilarHeaders()
    const { mzphz_userId: userId } = headers
    if (!userId || !qId) {
      window.location.href = link
      return
    }
    getEWMImg(userId, qId).then(res => {
      if (res.state === -1) {
        window.location.href = link
        return
      }
      this.setState({ ...res, userId, qId, files: [{ url: res.ewmUrl }] })
    })
  }

  onChange = files => {
    const { userId } = this.state

    if (files.length !== 0) {
      const file = files[0].file
      const { type, size } = file
      if (type !== "image/jpeg" && type !== "image/png") {
        Toast.info("只能上传jpg和png格式的图片")
        return
      }

      if (size / 1000 > 256) {
        Toast.info("图片大小不能大于256kb")
        return
      }
      const formData = new FormData()
      formData.append("userfile", file)
      formData.append("userId", userId)
      uploadEWM(formData).then(res => {
        const files = [
          { url: `${res.imgUrl}?${new Date().getTime()}`, id: Math.random() }
        ]
        this.setState({ files })
      })
    } else {
      this.setState({
        files
      })
    }
  }

  render() {
    const { state, qzName, qName, files } = this.state
    const { onChange } = this
    return (
      <div className="qz-page">
        {state !== -1 ? (
          <Modal
            title={`${qzName}-${qName}`}
            visible={state === 0}
            className="qz-modal"
          >
            {/* <img src={ewmUrl} alt="" className="qz-code-img" /> */}
            <ImagePicker
              className="qz-picker"
              files={files}
              onChange={onChange}
              length="1"
              disableDelete={state !== 1}
              selectable={files.length < 1}
            />
          </Modal>
        ) : null}
      </div>
    )
  }
}

export default QZ
